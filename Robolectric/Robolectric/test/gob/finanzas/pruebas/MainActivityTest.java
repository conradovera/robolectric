package gob.finanzas.pruebas;

import gob.finanzas.robolectric.MainActivity;
import gob.finanzas.robolectric.Principal;
import gob.finanzas.robolectric.R;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.shadows.ShadowActivity;
import org.robolectric.shadows.ShadowIntent;

import android.content.Intent;
import android.widget.Button;
import android.widget.EditText;

@RunWith(RobolectricTestRunner.class)
public class MainActivityTest {
	private MainActivity activity;
	private Button botonRedireccionar;
	private Button botonContar;
	private EditText contador;
	
	@Before
	public void init(){
		activity=Robolectric.buildActivity(MainActivity.class).create().get();
		botonRedireccionar=(Button) activity.findViewById(R.id.botonRedireccionar);
		botonContar=(Button) activity.findViewById(R.id.botonContar);
		contador=(EditText) activity.findViewById(R.id.contador);
	}
	
	@Test
	public void contar(){
		for(int i=0;i<20;i++){
			botonContar.performClick();
		}
		Assert.assertEquals(20+"", contador.getText().toString());
	}
	
	@Test
	public void siguienteActividad(){
		botonRedireccionar.performClick();
		ShadowActivity shadowActivity=Robolectric.shadowOf(activity);
		Intent intentIniciado=shadowActivity.getNextStartedActivity();
		ShadowIntent shadowIntent=Robolectric.shadowOf(intentIniciado);
		Assert.assertEquals(Principal.class.getName(), shadowIntent.getComponent().getClassName());
		//Assert.assertThat(shadowIntent.getComponent().getClassName(),CoreMatchers.equalTo(Principal.class.getName()));
	}
}
